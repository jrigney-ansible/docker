#test using a docker image of target platform

docker build -t ansible_target_ubuntu1610 .

docker run --rm -v ~/.ssh/id_rsa.pub:/home/ansible/.ssh/authorized_keys -p 2222:22 -it ansible_target_ubuntu1610 /bin/bash
