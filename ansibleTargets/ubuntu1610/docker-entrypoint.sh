#!/usr/bin/env bash
set -eo pipefail

# enable rsyslog daemon so that SSHD log file is created
#rsyslogd
mkdir -p /var/run/sshd

# start SSH daemon
/usr/sbin/sshd -D
